/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sc.stringclassmodification;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

/**
 *
 * @author lucifer
 */
public class App {

    private static final String TOOLS = "tools.jar";

    static {
        try {
            String javaHome = System.getProperty("java.home");
            String lib = javaHome + File.separator + (javaHome.contains("jre") ? ".." + File.separator + "lib" + File.separator + TOOLS : "lib" + File.separator + TOOLS);
            URL toolsPath = new File(lib).toURI().toURL();
            Utils.addURL(toolsPath);
        } catch (IOException ex) {

        }

    }

    public static void main(String[] args) throws Exception {
        ClassPool cp = ClassPool.getDefault();
        CtClass clz = cp.get(String.class.getName());
        CtMethod mtd = clz.getDeclaredMethod("codePointAt");
//        mtd.insertBefore("throw new RuntimeException();");
        mtd.insertBefore("return -1;");
        ModificationHelper.redefine(String.class, clz);
        String a = "2222";
        System.out.println(a.codePointAt(0));
    }
}
